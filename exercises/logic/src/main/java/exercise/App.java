package exercise;

class App {
    public static boolean isBigOdd(int number) {
        // BEGIN
        boolean isBigOddVariable;
        if (number >= 1001 && number % 2 != 0) {
            isBigOddVariable = true;
        } else {
            isBigOddVariable = false;
        }
        // END

        return isBigOddVariable;
    }

    public static void sayEvenOrNot(int number) {
        // BEGIN
        String even;
        if (number % 2 == 0) {
            even = "yes";
        } else {
            even = "no";
        }
        System.out.println(even);
        // END
    }

    public static void printPartOfHour(int minutes) {
        // BEGIN
        String res;
        if (minutes > 45) {
            res = "Fourth";
        } else if (minutes > 30) {
            res = "Third";
        } else if (minutes > 15) {
            res = "Second";
        } else {
            res = "First";
        }

        System.out.println(res);
        // END
    }
}
